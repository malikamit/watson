package com.example.demo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ibm.watson.developer_cloud.natural_language_understanding.v1.NaturalLanguageUnderstanding;
import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.AnalysisResults;
import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.AnalyzeOptions;
import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.CategoriesOptions;
import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.EntitiesOptions;
import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.Features;
import com.ibm.watson.developer_cloud.natural_language_understanding.v1.model.KeywordsOptions;
import com.ibm.watson.developer_cloud.service.security.IamOptions;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws IOException, TikaException {
		String apiKey = "OS-H01BZfLWtRrtPYXT8LP6IUt8IBNZSOfEYkPqbrnam";
		String apiUrl = "https://gateway-lon.watsonplatform.net/natural-language-understanding/api";

		IamOptions options = new IamOptions.Builder().apiKey(apiKey).build();
		NaturalLanguageUnderstanding naturalLanguageUnderstanding = new NaturalLanguageUnderstanding("2018-11-16",
				options);
		naturalLanguageUnderstanding.setEndPoint(apiUrl);

		String content = getResumeContent();

		System.out.println(content);

		EntitiesOptions entitiesOptions = new EntitiesOptions.Builder().emotion(false).sentiment(false).limit(20).build();

		KeywordsOptions keywordsOptions = new KeywordsOptions.Builder().emotion(false).sentiment(false).limit(20).build();
		
		CategoriesOptions categories = new CategoriesOptions();
		categories.setLimit(5);

		Features features = new Features.Builder().entities(entitiesOptions).keywords(keywordsOptions).categories(categories).build();

		AnalyzeOptions parameters = new AnalyzeOptions.Builder().text(content).features(features).build();

		AnalysisResults response = naturalLanguageUnderstanding.analyze(parameters).execute();
		System.out.println(response);

	}

	public static String getResumeContent() throws IOException, TikaException {
		Tika tika = new Tika();
		try (InputStream stream = new FileInputStream("C:\\Users\\amimalik0\\Desktop\\Resume.pdf")) {
			return tika.parseToString(stream);
		}
	}

}
